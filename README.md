# fruzzy - a fuzzy logic rust library

**fruzzy** is a fuzzy logic library written in the Rust programming language. It includes an inference engine that can be used to build Fuzzy Inference Systems. Please check the [Fuzzy Logic file](FUZZY.md) for info related to how fuzzy logic works in this library.

## Building

Make sure you have installed a Rust toolchain, be it stable or default. After that, you can include the library in the Cargo.toml of your project, like this:

```
[dependencies]
fruzzy = "0.1.0"
```

Now you will be able to use the library and build your project, with a simple cargo command:

```
$ cargo build
```

## Usage

Check out [USAGE.md](USAGE.md) for info on how to use the library.
