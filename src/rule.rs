use num::{Float, Integer};
use sets::Set;
use std::rc::Rc;

/// A logical 'var is set' premise
///
/// A logical premise that represents the association of a specific variable to a mathematical set.
/// It follows the basic layout of:
///
/// `X is A`
///
/// which represents that the variable X belongs fully (in the classical mathematical set) to A. In
/// fuzzy logic, this association can be either fully or partially, represented by a degree of
/// membership, stated as:
///
/// μ_A(x) = 0.9
///
/// which can be read as "the degree of membership of X in relation to the fuzzy set A is 0.9"
#[derive(Clone)]
pub struct Premise<T>
where
    T: Float + Integer,
{
    variable: T,
    set: Rc<Set<T>>,
    degree: f32,
}

/// A logical IF-THEN rule
///
/// A logical *Modus Ponens* rule of inference. It follows the basic layout of:
///
/// `IF antecedent THEN consequent
///
/// with both the antecedent and the consequent being logical premises. It states that if the
/// antecedent is true, then so it is for the consequent. From this, the inference can be made based
/// on already defined rules, in a combined rule base.
pub struct Rule<T>
where
    T: Float + Integer,
{
    antecedent: Vec<Premise<T>>,
    consequent: Vec<Premise<T>>,
}

impl<T> Rule<T>
where
    T: Float + Integer,
{
    fn set_antecedents(&mut self, ants: &[Premise<T>]) {
        self.antecedent = ants.to_vec();
    }

    fn set_consequents(&mut self, cons: &[Premise<T>]) {
        self.consequent = cons.to_vec();
    }

    fn antecedents(&self) -> &[Premise<T>] {
        &self.antecedent
    }

    fn consequents(&self) -> &[Premise<T>] {
        &self.consequent
    }
}
