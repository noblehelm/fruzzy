use fuzzification;
use num::{Float, Integer, Zero};
use std::cmp::Ord;

#[derive(Clone, Copy)]
enum MembershipFunction {
    Triangular,
    Trapezoidal,
    Piecewise,
    Sigmoidal,
    Gaussian,
    Singleton,
    Undefined,
}

/// A fuzzy set, determined by a given membership function
#[derive(Clone)]
pub struct Set<T>
where
    T: Float + Integer,
{
    nomenclature: String,
    zero: Vec<T>,
    one: Vec<T>,
    delta: Option<T>,
    membership_function: MembershipFunction,
}

impl<T> Set<T>
where
    T: Float + Integer,
{
    fn new(nomenc: &str) -> Self {
        Set {
            nomenclature: String::from(nomenc),
            zero: vec![],
            one: vec![],
            delta: None,
            membership_function: MembershipFunction::Undefined,
        }
    }

    fn triangular(&mut self, points: &[T]) {
        self.membership_function = MembershipFunction::Triangular;
        self.zero = vec![points[0], points[2]];
        self.one = vec![points[1]];
    }

    fn trapezoidal(&mut self, points: &[T]) {
        self.membership_function = MembershipFunction::Trapezoidal;
        self.zero = vec![points[0], points[3]];
        self.one = vec![points[1], points[2]];
    }

    fn piecewise(&mut self, points: &[T]) {
        self.membership_function = MembershipFunction::Piecewise;
        self.zero = vec![];
        self.one = vec![];
    }

    fn singleton(&mut self, point: T) {
        self.membership_function = MembershipFunction::Singleton;
        self.one = vec![point];
    }

    fn gaussian(&mut self, points: &[T]) {
        self.membership_function = MembershipFunction::Gaussian;
        self.zero = vec![points[0], points[2]];
        self.one = vec![points[1]];
    }

    fn mf(&self) -> MembershipFunction {
        self.membership_function
    }

    /// Fuzzify the value of X, returning the membership degree
    fn fuzzify(&self, x: T) -> T {
        match self.membership_function {
            MembershipFunction::Triangular => {
                fuzzification::trimf(x, self.zero[0], self.one[0], self.zero[1])
            }
            _ => {
                println!("Nothing to see here..");
                T::zero()
            }
        }
    }
}

/// Universe of discourse
///
/// Range of all values observed in a given measured variable.
pub struct Universe<T>
where
    T: Float + Integer,
{
    sets: Vec<Set<T>>,
    min: T,
    max: T,
}

impl<T> Universe<T>
where
    T: Float + Integer,
{
    fn new() -> Self {
        Universe {
            sets: vec![],
            min: T::zero(),
            max: T::zero(),
        }
    }

    fn sets(&mut self, s: &[Set<T>]) {
        self.sets = s.to_vec();
    }

    fn define_minmax(&mut self) {
        let mut min: T = T::zero();
        let mut max: T = T::zero();
        for (i, each) in self.sets.iter().enumerate() {
            if i == 0 {
                min = Ord::min(
                    *each.zero.iter().min().unwrap_or(&T::zero()),
                    *each.one.iter().min().unwrap_or(&T::zero()),
                );
                max = Ord::max(
                    *each.zero.iter().max().unwrap_or(&T::zero()),
                    *each.one.iter().max().unwrap_or(&T::zero()),
                );
            } else {
                min = Ord::min(
                    min,
                    Ord::min(
                        *each.zero.iter().min().unwrap_or(&T::zero()),
                        *each.one.iter().min().unwrap_or(&T::zero()),
                    ),
                );
                max = Ord::max(
                    max,
                    Ord::max(
                        *each.zero.iter().max().unwrap_or(&T::zero()),
                        *each.one.iter().max().unwrap_or(&T::zero()),
                    ),
                );
            }
        }
        self.min = min;
        self.max = max;
    }

    fn min(&self) -> T {
        self.min
    }

    fn max(&self) -> T {
        self.max
    }
}
