use num::{Float, Integer, One, Zero};

/// The Triangular membership function
///
/// A membership function that defines a fuzzy set in a Triangular shape
///
/// Parameters: a, b, c
///
/// a = where the function starts rising from 0
///
/// b = where the function reaches 0 again
///
/// c = the center piece of the function, where the membership degree is equal to 1
pub fn trimf<T: Float + Integer>(x: T, a: T, b: T, c: T) -> T {
    if x <= a || x > b {
        T::zero()
    } else if a < x && x <= c {
        (x - a) / (c - a)
    } else if c < x && x <= b {
        (b - x) / (b - c)
    } else {
        T::zero()
    }
}

/// The Trapezoidal membership function
///
/// A membership function that defines a fuzzy set in a Trapezoidal shape
///
/// Parameters: a, b, c, d
///
/// a = start of the first slope
///
/// b = end of the first slope, where the membership degree is equal to 1
///
/// c = start of the second and descending slope
///
/// d = end of the second slope, where the membership degree is equal to 0 again
pub fn trapmf<T: Float + Integer>(x: T, a: T, b: T, c: T, d: T) -> T {
    if x <= a || x > d {
        T::zero()
    } else if a < x && x <= b {
        (x - a) / (b - a)
    } else if b < x && x <= c {
        T::one()
    } else if c < x && x <= d {
        (d - x) / (d - c)
    } else {
        T::zero()
    }
}

/// The Sigmoidal membership function
///
/// A membership function that defines a fuzzy set like a Sigmoid curve
///
/// Parameters: a, b
///
/// a = where the function reaches half the membership degree
///
/// b = the "openness" of the curve
pub fn sigmf<T: Float + Integer>(x: T, c: T, b: T) -> T {
    T::one() / T::one() + (-c * (x - b)).exp()
}

/// The Gaussian membership function
///
/// A membership function that defines a fuzzy set like a Gaussian curve.
///
/// Parameters: c, rho
///
/// c = mean/average where the degree of membership is equal to 1
///
/// rho = standard deviation
pub fn gaussmf<T: Float + Integer>(x: T, c: T, rho: T) -> T {
    (-(x - c).powi(2) / T::from(2).unwrap() * rho.powi(2)).exp()
}

/// The S membership function
///
/// A membership function that defines a fuzzy set in a S shape.
///
/// Parameters: a, b
///
/// a = where the function starts to rise from 0
///
/// b = where the function reaches 1
pub fn smf<T: Float + Integer>(x: T, a: T, b: T) -> T {
    if x <= a {
        T::zero()
    } else if a < x && x <= (a + b) / T::from(2).unwrap() {
        T::from(2).unwrap() * ((x - b) / (b - a)).powi(2)
    } else if (a + b) / T::from(2).unwrap() < x && x <= b {
        T::one() - T::from(2).unwrap() * ((x - b) / (b - a)).powi(2)
    } else if x > b {
        T::one()
    } else {
        T::zero()
    }
}

/// The Z membership function
///
/// A membership function that defines a fuzzy set in a Z shape
///
/// Parameters: a, b
///
/// a = where the function starts to descend
///
/// b = where the function ends the descend, with membership degree being equal to 0
pub fn zmf<T: Float + Integer>(x: T, a: T, b: T) -> T {
    if x <= a {
        T::one()
    } else if a < x && x <= (a + b) / T::from(2).unwrap() {
        T::one() - T::from(2).unwrap() * ((x - a) / (b - a)).powi(2)
    } else if (a + b) / T::from(2).unwrap() < x && x <= b {
        T::from(2).unwrap() * ((x - a) / (b - a)).powi(2)
    } else if x > b {
        T::zero()
    } else {
        T::zero()
    }
}

/// The singleton membership function
///
/// A membership function that defines a fuzzy set where only a single value has the degree of
/// membership equal to 1
///
/// Parameters: a
///
/// a = where the function is equal to 1
fn singletonmf<T: Float + Integer>(x: T, p: &T) -> T {
    match x {
        p => T::one(),
        _ => T::zero(),
    }
}
