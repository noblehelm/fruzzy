extern crate num;

pub mod fuzzification;
pub mod rule;
pub mod sets;
